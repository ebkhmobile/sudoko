
package sudoko.azad.com.sudoko;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import java.lang.reflect.Type;
import java.util.TimerTask;

public class About extends Activity {
    TextView txt ;
    Typeface typeface;
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.about);
       typeface = Typeface.createFromAsset(getAssets(),"Behdad-Regular.ttf");
       txt = (TextView) findViewById(R.id.dgisoft);
       txt.setTypeface(typeface);
   }
}
