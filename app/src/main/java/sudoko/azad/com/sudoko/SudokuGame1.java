package sudoko.azad.com.sudoko;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class SudokuGame1 extends Activity implements OnClickListener {
	   
	   Typeface behdad ;
	   private static final String TAG = "Sukgludflkgfdkhd";
	   /** Called when the activity is first created. */
	   @Override
	   public void onCreate(Bundle savedInstanceState) {
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.main);

	      // Set up click listeners for all the buttons
	      View continueButton = findViewById(R.id.continue_button);
	      continueButton.setOnClickListener(this);
	      View newButton = findViewById(R.id.new_button);
	      newButton.setOnClickListener(this);
	      View aboutButton = findViewById(R.id.about_button);
	      aboutButton.setOnClickListener(this);
	      View exitButton = findViewById(R.id.exit_button);
	      exitButton.setOnClickListener(this);
          behdad = Typeface.createFromAsset(getAssets(),"Behdad-Regular.ttf");

	   }
	   


	   // ...
	   public void onClick(View v) {
	      switch (v.getId()) {
	      case R.id.about_button:
              Dialog dialog = new Dialog(SudokuGame1.this);
              dialog.setTitle("درباره ما ");
              dialog.setContentView(R.layout.about);
              dialog.show();
	      
	      break;
	      case R.id.new_button:
	         openNewGameDialog();
	         break;
	      
	      
	      case R.id.exit_button:
	         finish();
	         break;
	      
	      
	      }
	   }
	   
	   
	   
	   @Override
	   public boolean onCreateOptionsMenu(Menu menu) {
	      super.onCreateOptionsMenu(menu);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.menu, menu);
	      return true;
	   }
	   

	   
	   @Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      switch (item.getItemId()) {
	      case R.id.settings:
	         startActivity(new Intent(this, Prefs.class));
	         return true;
	      // More items go here (if any) ...
	      }
	      return false;
	   }
	   

	   
	   /** Ask the user what difficulty level they want */
	   private void openNewGameDialog() {
	      new AlertDialog.Builder(this)
	           .setTitle("سطح بازی :")
	           .setItems(R.array.difficulty,
	            new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialoginterface,
	                     int i) {
	                  startGame(i);
	               }
	            })
	           .show();
	   }
	   
	   /** Start a new game with the given difficulty level */
	   private void startGame(int i) {
		      Log.d(TAG, "clicked on " + i);
		      Intent intent = new Intent(SudokuGame1.this, Game.class);
		      intent.putExtra(Game.KEY_DIFFICULTY, i);
		      startActivity(intent);
		   }
		   

	   
	   
	}
