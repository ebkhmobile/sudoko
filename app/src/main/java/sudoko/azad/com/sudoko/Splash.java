package sudoko.azad.com.sudoko;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Mahdi on 21/05/2017.
 */
public class Splash extends Activity {
    Thread thread;
    TextView txt ;
    ImageView splash ;
    Typeface behdad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        splash = (ImageView) findViewById(R.id.splashimage);
        splash.setBackgroundDrawable(getResources().getDrawable(R.drawable.splashsudoku));
        behdad = Typeface.createFromAsset(getAssets(), "Behdad-Regular.ttf");
        txt = (TextView) findViewById(R.id.splashtext);
        txt.setTypeface(behdad);
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    Intent intent = new Intent(Splash.this,SudokuGame1.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }
}
